var $grid = $('.grid');
var file = document.getElementById("file");

function gridItem(item, i) {
    var div = document.createElement('div'),
        a = document.createElement('section'),
        image = document.createElement('img'),
        figure = document.createElement('figure'),
        figCapture = document.createElement('figcaption'),
        i1 = document.createElement('i'),
        i2 = document.createElement('i'),
        i3 = document.createElement('i'),
        span1 = document.createElement('span'),
        span2 = document.createElement('span'),
        span3 = document.createElement('span'),
        imgSrc = document.createAttribute("src"),
        dataPop = document.createAttribute("data-pop"),
        index = i + 1;

    div.setAttributeNode(dataPop);
    image.setAttributeNode(imgSrc);
    dataPop.value = index;
    imgSrc.value = item.path;


    span1.innerHTML = item.comments.length;
    span2.innerHTML = item.likes;
    span3.innerHTML = item.dislikes;

    a.classList.add("pop");
    div.classList.add("grid-item", "elem", item.width);
    i1.classList.add("sprite", "comment");
    i2.classList.add("sprite", "like");
    i3.classList.add("sprite", "dislike");

    a.appendChild(figure);
    div.appendChild(a)
    figure.appendChild(image);
    figure.appendChild(figCapture);
    figCapture.appendChild(i1);
    figCapture.appendChild(i2);
    figCapture.appendChild(i3);
    i1.appendChild(span1);
    i2.appendChild(span2);
    i3.appendChild(span3);

    return div
}

function gridRender() {
    var grid = document.getElementsByClassName("grid")[0]
    popUp.forEach(function (item, i) {
        grid.prepend(gridItem(item, i));
    })
}
gridRender()

function popItem(item, i) {

    var div = document.createElement('div'),
        close = document.createElement('button'),
        box = document.createElement('div'),
        comments = document.createElement('div'),
        likes = document.createElement('div'),
        likes2 = document.createElement('div'),
        image = document.createElement('img'),
        index = i + 1,
        divId = document.createAttribute("id"),
        dataSrc = document.createAttribute("data-src"),
        figure = document.createElement('figure'),
        figCapture = document.createElement('figcaption'),
        imgSrc = document.createAttribute("src"),
        i1 = document.createElement('i'),
        i2 = document.createElement('i'),
        i3 = document.createElement('i'),
        span1 = document.createElement("span"),
        span2 = document.createElement("span"),
        comentBody = document.createElement("div"),
        commentsTitle = document.createElement("h2"),
        commentsArea = document.createElement('div'),
        commentsInput = document.createElement('input'),
        placeholder = document.createAttribute("placeholder"),
        type = document.createAttribute("type"),
        commentsTextarea = document.createElement('textarea'),
        placeholderTextarea = document.createAttribute("placeholder"),
        countComent = document.createElement('span'),
        button = document.createElement('button');

    item.comments.forEach(function (com) {
        var commentbody = document.createElement('div'),
            commenName = document.createElement('div'),
            commentItem = document.createElement('div'),
            commentdate = document.createElement('div');
        commentbody.classList.add("comment-body");
        commentdate.classList.add("date");
        commenName.classList.add("name");
        commentItem.classList.add("comment-item");
        commentsArea.appendChild(commentbody)
        commentbody.appendChild(commenName)
        commentbody.appendChild(commentdate)
        commentbody.appendChild(commentItem)
        commenName.innerHTML = com.author
        commentdate.innerHTML = com.date
        commentItem.innerHTML = com.commentText
    })

    commentsTextarea.setAttributeNode(placeholderTextarea);
    commentsInput.setAttributeNode(placeholder);
    commentsInput.setAttributeNode(type);
    image.setAttributeNode(imgSrc);
    div.setAttributeNode(divId);
    div.setAttributeNode(dataSrc);

    likes.classList.add("likes", "likeC");
    likes2.classList.add("likes", "dislikeC");
    button.classList.add("comment-but");
    comentBody.classList.add("comment-body");
    i1.classList.add("sprite", "like");
    i2.classList.add("sprite", "dislike", item.width);
    close.classList.add("mfp-close");
    box.classList.add("box");
    comments.classList.add("comments");
    countComent.classList.add("count");
    commentsArea.classList.add("comments-area");
    div.classList.add("white-popup", "mfp-hide");

    span1.innerHTML = item.likes;
    span2.innerHTML = item.dislikes;
    countComent.innerHTML = item.comments.length;
    commentsTitle.innerHTML += "Comments ";

    imgSrc.value = item.path;
    divId.value = "pop-" + index;
    imgSrc.value = item.path;
    dataSrc.value = index;
    type.value = "text";
    placeholder.value = "Type your nickname here...";
    placeholderTextarea.value = "Write your comment here...";

    comments.appendChild(commentsTitle);
    commentsTitle.appendChild(countComent)
    comments.appendChild(commentsArea);
    comments.appendChild(commentsInput);
    comments.appendChild(commentsTextarea);
    comments.appendChild(button);
    figure.appendChild(image);
    figure.appendChild(figCapture);
    figCapture.appendChild(box);
    box.appendChild(likes);
    box.appendChild(likes2);
    likes.appendChild(i1);
    likes2.appendChild(i2);
    i1.appendChild(span1);
    i2.appendChild(span2);
    div.appendChild(figure);
    div.appendChild(comments);
    div.appendChild(close);

    return div
}

function popUprender(item, i) {
    var popUpOverlay = document.getElementById("popUpOverlay")
    popUp.forEach(function (item, i) {
        popUpOverlay.append(popItem(item, i));
    })
}
popUprender()

function addNew(src, filesName) {
    popUp.push({
        path: src,
        likes: 0,
        dislikes: 0,
        comments: []
    })
    var overlay = document.getElementById("popUpOverlay")
    var id = document.querySelectorAll("div.grid-item").length - 1;

    var $items = gridItem(popUp[id], id)
    $grid.append($items).packery('prepended', $items);
    overlay.append(popItem(popUp[id], id));
    [].forEach.call(document.querySelectorAll("div.elem"), function (el) {
        el.addEventListener('click', createPopup, false)
    });
    [].forEach.call(document.querySelectorAll("button.comment-but"), function (el) {
        el.addEventListener('click', addComment, false)
    });
    [].forEach.call(document.querySelectorAll("button.mfp-close"), function (el) {
        el.addEventListener('click', closeOverlay, false)
    });
    [].forEach.call(document.querySelectorAll("div.likeC"), function (el) {
        el.addEventListener('click', addLike, false)
    });
    [].forEach.call(document.querySelectorAll("div.dislikeC"), function (el) {
        el.addEventListener('click', addDisLike, false)
    });

};

function createPopup() {
    var id = this.getAttribute("data-pop") - 1,
        el = document.querySelectorAll("div.mfp-hide"),
        overlay = document.getElementById("popUpOverlay");
    el.forEach(function (elem) {
        elem.style.display = "none"
    })
    overlay.style.display = "flex"

    el[id].style.display = "flex"
}

function reRender(index) {
    var index2 = index - 1,
        $items = gridItem(popUp[index2], index2),
        element = document.querySelectorAll("[data-pop='" + index + "']")[0]
    element.remove()
    $grid.append($items).packery('prepended', $items);
    
    [].forEach.call(document.querySelectorAll("div.elem"), function (el) {
        el.addEventListener('click', createPopup, false)
    });
    [].forEach.call(document.querySelectorAll("button.comment-but"), function (el) {
        el.addEventListener('click', addComment, false)
    });
    [].forEach.call(document.querySelectorAll("button.mfp-close"), function (el) {
        el.addEventListener('click', closeOverlay, false)
    });
}

function closeOverlay(event) {
  
    event.stopPropagation();
    overlay = document.getElementById("popUpOverlay");
    overlay.style.display = "none"
    
}

function addComment(e) { 
    var date = new Date(),
        hours = date.getHours(),
        min = date.getMinutes(),
        day = date.getDate(),
        bt = e.target,
        parent = bt.parentNode,
        parent2 = bt.parentNode.parentNode,
        parentAtrr = parseFloat(parent2.getAttribute("data-src")) - 1,
        textArea = bt.previousElementSibling,
        text = bt.previousElementSibling.value,
        inputName = textArea.previousElementSibling,
        name = inputName.value,
        commentNode = document.createElement("div"),
        len = parseFloat(parent.getElementsByClassName("count")[0].innerHTML);
    commentNode.classList.add("comment-body");
    commentNode.innerHTML = "<div class='name'>By " + name + "</div><div class='date'> Today " + hours + ":" + min + " PM</div><div class='comment-item'>" + text + "</div>";
    if (text.value != '' && inputName.value != '') {
        popUp[parentAtrr].comments.push({
            author: inputName.value,
            commentText: textArea.value,
            date: [hours, min, day]
        })
        parent.getElementsByClassName("comments-area")[0].appendChild(commentNode)
        textArea.value = '';
        inputName.value = '';
        len++
        parent.getElementsByClassName("count")[0].innerHTML = len
        reRender(parentAtrr+1)
    }    
}

function handleFileSelect(evt) {
    var files = evt.target.files,
        filesName = evt.target.files[0].name,
        img = new Image();
    img.src = URL.createObjectURL(evt.target.files[0]);
    addNew(img.src, filesName)
}

function addLike(e) {
    var children = parseFloat(this.childNodes[0].childNodes[0].innerHTML),
        id = parseFloat(e.currentTarget.parentNode.parentNode.parentNode.parentNode.getAttribute("data-src"))
    children++
    this.childNodes[0].childNodes[0].innerHTML = children
    console.log(children)
    popUp[id-1].likes+=1
    reRender(id)
    this.nextSibling.classList.remove('active')
    this.classList.add('active')    
}
function addDisLike(e) {
    var children = parseFloat(this.childNodes[0].childNodes[0].innerHTML),
        id = parseFloat(e.currentTarget.parentNode.parentNode.parentNode.parentNode.getAttribute("data-src"))
    children++
    this.childNodes[0].childNodes[0].innerHTML = children
    console.log(children)
    popUp[id-1].dislikes+=1
    reRender(id)
    this.previousSibling.classList.remove('active')
    this.classList.add('active') 
}

file.addEventListener('change', handleFileSelect, false);
[].forEach.call(document.querySelectorAll("button.comment-but"), function (el) {
    el.addEventListener('click', addComment, false)
});
[].forEach.call(document.querySelectorAll("button.mfp-close"), function (el) {
    el.addEventListener('click', closeOverlay, true)
});
[].forEach.call(document.querySelectorAll("div.elem"), function (el) {
    el.addEventListener('click', createPopup, true)
});
[].forEach.call(document.querySelectorAll("div.likeC"), function (el) {
    el.addEventListener('click', addLike, false)
});
[].forEach.call(document.querySelectorAll("div.dislikeC"), function (el) {
    el.addEventListener('click', addDisLike, false)
});


$('.grid').packery({
    itemSelector: '.grid-item',
    gutter: 10,
    horizontal: true,
    resize: true
});
